#!/bin/bash

echo "Press enter to create control plane"
read -n1
oc new-project my-smcp
oc create -f istio/01_istio_control_plane.yaml

echo "Press enter to create member role in my-demo"
read -n1
oc new-project my-demo
oc project my-smcp
oc create -f istio/02_istio_member_role.yaml

oc project my-demo
echo "Press enter to deploy demo app"
read -n1
for component in istio/app/*; do
    oc create -f $component
done

console=$(echo https://$(oc get route -n my-smcp kiali -o 'jsonpath={.spec.host}'))
echo "Please open the console at ${console} and press enter afterwards."
echo "Select Graph, the my-demo namespace, using versioned app graph, request percentage, and animated traffic."
read -n1

echo "Press start curl_customer_quiet.sh to start generating load on the app to make it visible"
echo "Use a second terminal for that and press enter."
read -n1

jaeger=$(echo https://$(oc get route -n my-smcp jaeger -o 'jsonpath={.spec.host}'))
echo "Open the jaeger console at ${jaeger} and press enter afterwards."
echo "Select the my-demo namesapce, service my-demo -> customer"
echo "Shows 20 most recent invocations of customer service"
read -n1

echo "Go back to Kiali console and show overview."
echo "Press enter to setup a Destinationrule and a VirtualService to"
echo "send 80% of traffic to v1, 20% of traffic to v2 and 0% to v3."
echo " -- have patience --"
read -n1
oc create -f istio/04_destination_rule.yaml
oc create -f istio/05_virtualservice.yaml


echo "Press enter to switch traffic to v2 mostly and a bit to v1 and v3"
read -n1
oc apply -f istio/05.5_virtualservice.yaml

echo "You can also match to traffic and do canary releases this way."
echo "You can mirror traffic to do a dark launch as well."
echo "Press enter to clean up the virtualservice."
read -n1
oc  delete virtualservice recommendation -n my-demo

echo "Stop the script in the other shell, and run the curl_customer_preference_quiet.sh script instead."
echo "This will creat traffic from within the mesh and from outside the mesh."
echo "At this moment, everything will work."
echo "Go to the Mesh object, and enable Dataplane security (can also be done through YAML, of course)."
echo "It is also possible to phase in mTLS through a PERMISSIVE setting, and be more specific on which pods are required to use it."
echo "For now, this will have to suffice though."
echo "Switch the dataplane security back off."
read -n1
echo "End of demo"
