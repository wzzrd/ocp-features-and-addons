#!/bin/bash

echo "rosa install addon --cluster vb-test-01 cluster-logging-operator"
rosa install addon --cluster vb-test-01 cluster-logging-operator

echo "Can also be done through OCM"
echo "We can use this operator to either set up forwarding to CloudWatch, log on the"
echo "cluster itself, or forward to another external logging sink."
echo "We have now configured it to forward to CloudWatch only."
