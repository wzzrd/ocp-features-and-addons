#!/bin/bash

echo "Have you installed the pipelines operator?"
echo "Hit enter for yes:"
read -n1

git clone https://github.com/wzzrd/tekton-cd-demo
oc new-project demo

cd tekton-cd-demo
git checkout pipelines-1.4
./demo.sh install
