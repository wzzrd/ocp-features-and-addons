#!/bin/sh

export CURL_POD=$(oc get pods -n my-demo -l app=curl | grep curl | awk '{ print $1}' )
export CUSTOMER_POD=$(oc get pods -n my-demo -l app=customer | grep customer | awk '{ print $1}' )

echo "Curl from outside mesh"
oc exec -n my-demo $CURL_POD -- curl -sv http://preference:8080 
echo; echo;
echo "Curl from inside mesh"
oc exec -n my-demo $CUSTOMER_POD -c customer -- curl -sv http://preference:8080 


