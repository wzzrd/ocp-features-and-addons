#!/bin/bash

echo "'Installed operators' will change when you hit enter"
read -n1

echo "oc create -f operator_group.yaml"
oc create -f operator_group.yaml
echo "oc create -f elastic_operator.yaml"
oc create -f elastic_operator.yaml

echo "Install kiali, jaeger and mesh afterwards."
