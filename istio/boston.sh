#!/bin/bash

export INGRESS_GATEWAY=$(oc get route -n my-smcp istio-ingressgateway -o 'jsonpath={.spec.host}')
curl -H "user-location: Boston" http://${INGRESS_GATEWAY}/
