#!/bin/bash

read -p "Sysdig access key: " access_key

echo "Setting up sysdig now."
oc adm new-project sysdig-agent --node-selector='app=sysdig-agent'
oc project sysdig-agent
oc create serviceaccount sysdig-agent
oc adm policy add-scc-to-user privileged -n sysdig-agent -z sysdig-agent
oc adm policy add-cluster-role-to-user cluster-reader -n sysdig-agent -z sysdig-agent


oc create secret generic sysdig-agent --from-literal=access-key=${access_key} -n sysdig-agent

oc create -f sysdig-agent-configmap.yaml
oc create -f sysdig-agent-service.yaml
oc create -f sysdig-agent-daemonset-v2.yaml

echo "this will take a while to complete..."
echo "Open https://eu1.app.sysdig.com"
echo "Sysdig will run on the 'bigger' machinepool / machineset"
