#!/bin/bash

echo "rosa create machinepool -c vb-test-01 --enable-autoscaling --instance-type m5.2xlarge --max-replicas 9 --min-replicas 3 --name bigger --labels app=sysdig-agent"
rosa create machinepool -c vb-test-01 --enable-autoscaling --instance-type m5.2xlarge --max-replicas 9 --min-replicas 3 --name bigger --labels app=sysdig-agent"

echo "One instance per zone, so minimum of three, but autoscaling to maximum of 9"
echo "We can change this after creation, of course, by running:"
echo "   rosa edit machinepool --name bigger"
echo 
echo "Check the 'machines' and 'machinesets' pages"
