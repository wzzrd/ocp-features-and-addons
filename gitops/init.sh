#!/bin/bash

echo "Set up 
echo "    Application Name: cluster-configs"
echo "    Project: default"
echo "    Sync Policy: Manual"
echo "    Repository URL: https://github.com/wzzrd/openshift-gitops-getting-started
echo "    Revision: HEAD"
echo "    Path: cluster"
echo "    Destination: https://kubernetes.default.svc"
echo "    Namespace: default"
echo "    Directory Recurse: checked"
"
echo "Press enter when ready"
read -n1
echo "Sync the project, and check the Overview page for a message about ArgoCD"
echo "being out of sync (integrated) and an additional link to the blog."
echo "Message disappears after a while"
read -n1

echo "Create another app:"
echo "    Application Name: spring-petclinic"
echo "    Project: default
echo "    Sync Policy: Automatic"
echo "    Self-heal: checked"
echo "    Repository URL: https://github.com/siamaksade/openshift-gitops-getting-started"
echo "    Revision: HEAD"
echo "    Path: app"
echo "    Destination: https://kubernetes.default.svc"
echo "    Namespace: spring-petclinic"
echo "    Directory Recurse: checked"
"
echo "Press enter when ready"
echo "Also possible: oc create -f argo_app.yaml"
read -n1
