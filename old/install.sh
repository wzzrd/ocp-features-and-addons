#!/bin/bash

# install cluster:
rosa create cluster --region=eu-west-1 --version 4.7.9 --multi-az -c vb-test-03 --enable-autoscaling --watch        

# create admin:
rosa create admin --cluster=vb-test-03

# update cluster:
rosa upgrade cluster --cluster vb-test-03 --version 4.7.16 --schedule-date 2021-06-30 --schedule-time 05:00

# install github auth:
https://asciinema.org/a/AjI0v6d175JBjqr7HGwBXQXZq

# install crossplane
kubectl create namespace crossplane-system
helm repo add crossplane-stable https://charts.crossplane.io/stable
helm repo update
helm install crossplane --namespace crossplane-system crossplane-stable/crossplane --version 1.2.3 --set securityContextCrossplane.runAsUser=1000900001,securityContextCrossplane.runAsGroup=1000900001,securityContextRBACManager.runAsUser=1000900001,securityContextRBACManager.runAsGroup=1000900001
oc apply -f aws-provider.yaml
oc apply -f aws-secret.yaml
